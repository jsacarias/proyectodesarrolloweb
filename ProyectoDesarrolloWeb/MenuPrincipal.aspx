﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MenuPrincipal.aspx.cs" Inherits="ProyectoDesarrolloWeb.MenuPrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title></title>
</head>
    <body background="https://images.clarin.com/2020/05/08/arrastre-esta-foto-a-su___aYkw3W165_1200x0__1.jpg
">
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="Button1" runat="server" Text="Catalogo de vecinos" class="btn btn-warning" OnClick="Button1_Click"/>
            <asp:Button ID="Button2" runat="server" Text="Autorizacion de visitas" class="btn btn-warning" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Text="Generar Token" class="btn btn-warning" OnClick="Button3_Click"/>
            <asp:Button ID="Button4" runat="server" Text="Confirmar Token" class="btn btn-warning" OnClick="Button4_Click"/>
            <asp:Button ID="Button5" runat="server" Text="Bloqueo de visitas" class="btn btn-warning" OnClick="Button5_Click"/>
            <asp:Button ID="Button6" runat="server" Text="Lista Negra de Vecinos" class="btn btn-warning" OnClick="Button6_Click"/>
            <asp:Button ID="Button7" runat="server" Text="ID de visitas" class="btn btn-warning" OnClick="Button7_Click"/>
            <asp:Button ID="Button8" runat="server" Text="Reporte de visitas diarias" class="btn btn-warning" OnClick="Button8_Click"/>
            <asp:Button ID="Button9" runat="server" Text="Administracion de accesos" class="btn btn-warning"/>
            <asp:Button ID="Button10" runat="server" Text="Creacion de Roles y usuarios" class="btn btn-warning" OnClick="Button10_Click"/>
            

                

        </div>
    </form>
</body>
</html>
