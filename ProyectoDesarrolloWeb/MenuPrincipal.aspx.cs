﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoDesarrolloWeb
{
    public partial class MenuPrincipal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("CatalogoVecinos.aspx?");

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("AutorizarVisitas.aspx?");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("GenerarToken.aspx?");
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Response.Redirect("Confirmar Token.aspx?");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Response.Redirect("BloqueoVisitas.aspx?");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListaNegra.aspx?");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Redirect("IdentificacioVisita.aspx?");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReporteVisitas.aspx?");
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            Response.Redirect("RolesUsuarios.aspx?");
        }
    }
}