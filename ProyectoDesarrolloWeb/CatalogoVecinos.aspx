﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CatalogoVecinos.aspx.cs" Inherits="ProyectoDesarrolloWeb.CatalogoVecinos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title></title>
    
    <style type="text/css">
        #form1 {
            height: 197px;
        }
        .form-control {
            width: 502px;
        }
    </style>
</head>
<body background="https://www.solofondos.com/wp-content/uploads/2015/11/fondo_r1_c1.jpg">
    <form id="form1" runat="server">
        
        <div>
            <center>
            <div><h3>Nombre: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox1" runat="server" Width="364px" Height="30px"></asp:TextBox></div>
            <div><h3>Apellido: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox2" runat="server" Width="369px" Height="27px"></asp:TextBox></div>
            <div><h3>DPI: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox3" runat="server" Width="371px" Height="29px"></asp:TextBox></div>
            <div><h3>Telefono: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox4" runat="server" Width="371px" Height="30px"></asp:TextBox></div>
            <div><h3>Celular: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox5" runat="server" Width="370px" Height="28px"></asp:TextBox></div>
            <div><h3>No. de Residencia: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox6" runat="server" Width="370px" Height="26px"></asp:TextBox></div>
            <div><h3>No. de Integrantes: <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox7" runat="server" Width="368px" Height="30px"></asp:TextBox></div>
            <div><h3>Nombre Conyugue <span class="badge badge-secondary"></span></h3><asp:TextBox ID="TextBox8" runat="server" Width="368px" Height="27px"></asp:TextBox></div>
            <br />
            <br />
                </center>
            <div>
                
                <asp:Button ID="Button1" runat="server" Text="Limpiar" class="btn btn-success btn-lg float-right" OnClick="Button1_Click"/>
                <asp:Button ID="Button2" runat="server" Text="Eliminar" class="btn btn-success btn-lg float-right" OnClick="Button2_Click"/>
                <asp:Button ID="Button3" runat="server" Text="Actualizar Datos" class="btn btn-success btn-lg float-right" OnClick="Button3_Click"/>
                <asp:Button ID="Button4" runat="server" Text="Nuevo Vecino" class="btn btn-success btn-lg float-right" OnClick="Button4_Click"/>
                    
            </div>
        </div>
    
    </form>
</body>
</html>
